<?php
require_once ("../../include/initialize.php");
	 if (!isset($_SESSION['ACCOUNT_ID'])){
      redirect(web_root."admin/index.php");
     }

$action = (isset($_GET['action']) && $_GET['action'] != '') ? $_GET['action'] : '';

switch ($action) {
	case 'add' :
	doInsert();
	break;
	
	case 'edit' :
	doEdit();
	break;
	
	case 'delete' :
	doDelete();
	break;

	case 'photos' :
	doupdateimage();
	break;

 
	}
   
	function doInsert(){
		if(isset($_POST['save'])){


		//if ($_POST['DEPARTMENT_NAME'] == "" OR $_POST['DEPARTMENT_DESC'] == "" ) {
		if ($_POST['DEPARTMENT_NAME'] == "" OR $_POST['DEPARTMENT_DESC'] == "" OR $_POST['DEPT_ID'] =="" ) {
			$messageStats = false;
			message("All field is required!","error");
			redirect('index.php?view=add');
		}else{	
			$net = New Network(); 
		    $net->NET_NAME 		= $_POST['DEPARTMENT_NAME'];
			$net->NET_LEADER		= $_POST['DEPARTMENT_DESC']; 
			$net->DEPT_ID			= $_POST['DEPT_ID']; 
			$net->create();

			// $autonum = New Autonumber(); 
			// $autonum->auto_update(2);

			message("New [". $_POST['DEPARTMENT_NAME'] ."] created successfully!", "success");
			redirect("index.php");
			
		}
		}

	}

	function doEdit(){
	if(isset($_POST['save'])){ 

 		$desc = $_POST['DESCRIPTION'];

			$net = New Network(); 
			$net->NET_NAME		= $_POST['DEPARTMENT_NAME'];
			$net->NET_LEADER		= $desc;
			$net->DEPT_ID			= $_POST['DEPT_ID']; 			
			//$res = $net->update($_POST['DEPT_ID']);

			  message("[". $_POST['DEPARTMENT_NAME'] ."] has been updated!", "success");
			redirect("index.php");
			 
			
		}
	}


	function doDelete(){
		
	 
				$id = 	$_GET['id'];

				$net = New Network();
	 		 	$net->delete($id);
			 
			message("Network already Deleted!","info");
			redirect('index.php');
		 
		
	}

	 
 
?>